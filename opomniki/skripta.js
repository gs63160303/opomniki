window.addEventListener('load', function() {
	//stran nalozena
	
	// izvedi prijavo
	var izvediPrijavo = function() {
		// pridobimo objekt preko IDja
		var uporabnik = document.querySelector("#uporabnisko_ime").value;
		document.querySelector("#uporabnik").innerHTML = uporabnik;
		
		// pridobimo objekt preko class
		document.querySelector(".pokrivalo").style.visibility = "hidden";
	}
	document.querySelector("#prijavniGumb").addEventListener('click', izvediPrijavo);
	
	// dodajanje opomnikov
	var dodajOpomnik = function() {
		// preberi naziv opomnika
		var naziv_opomnika = document.querySelector("#naziv_opomnika").value;
		// nastavi na prazno
		document.querySelector("#naziv_opomnika").value = "";
		// preberi cas opomnika
		var cas_opomnika = document.querySelector("#cas_opomnika").value;
		// nastavi na prazno
		document.querySelector("#cas_opomnika").value = "";
		
		var opomniki = document.querySelector("#opomniki");
		opomniki.innerHTML += "\
			<div class='opomnik senca rob'> \
				<div class='naziv_opomnika'>" + naziv_opomnika + "</div> \
				<div class='cas_opomnika'>Opomnik cez <span>" +
					cas_opomnika + "</span> sekund.</div>\
					</div>";
	}
	document.querySelector("#dodajGumb").addEventListener('click', dodajOpomnik);
		
	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
		
		for (i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);
	
			//TODO: 
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
			if (cas == 0) {
				var naziv_opomnika = opomnik.querySelector(".naziv_opomnika").innerHTML;
				alert("Opomnik!\n\nZadolzitev '" + naziv_opomnika +
					"' je potekla");
				document.querySelector("#opomniki").removeChild(opomnik);
			} else {
				casovnik.innerHTML = cas - 1;
			}
		}
	}
	setInterval(posodobiOpomnike, 1000);
	
});